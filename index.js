let postContent = [];

const getPosts = () => {
  fetch("https://apis.scrimba.com/jsonplaceholder/posts")
    .then((res) => res.json())
    .then((data) => {
      postContent = data.slice(0, 5);
      renderPosts();
    });
};

const renderPosts = () => {
  let postsHtml = "";
  
  for (let post of postContent) {
    postsHtml += `
                  <div class="card">
                      <h3 class="post-title">${post.title}</h3>
                      <p class="post-body">${post.body}</p>
                  </div>
    `;
  }
  document.querySelector(".posts").innerHTML = postsHtml;
};

document.querySelector("form").addEventListener("submit", (e) => {
  e.preventDefault();
  const title = document.querySelector("#title-input").value;
  const body = document.querySelector("#body-input").value;
  const data = {
    title: title,
    body: body,
  };
  post(data);
});

const post = (data) => {
  const options = {
    method: "POST",
    body: JSON.stringify(data),
    headers: {
      "Content-Type": "application/json",
    },
  };

  fetch("https://apis.scrimba.com/jsonplaceholder/posts", options)
    .then((res) => res.json())
    .then((data) => {
      postContent.unshift(data)
      renderPosts()
      document.querySelector("form").reset();
    });
    
};

getPosts();
